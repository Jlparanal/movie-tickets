@startuml

actor User
actor Administrator
participant "User Interface" as UI
participant "Movie Database" as MovieDB
participant "Reservation System" as Reservation
participant "Payment Gateway" as Payment
participant "Admin Interface" as AdminUI

==Search==
User -> UI: Search for movie
UI -> MovieDB: Find movies by title
MovieDB -> MovieDB: Search movie database
MovieDB --> UI: Return list of matching movies

==View Movie Details==
User -> UI: Select movie
UI -> MovieDB: Get movie details
MovieDB -> MovieDB: Retrieve movie details
MovieDB --> UI: Return movie details

==Selects a Showtime==
User -> UI: Select showtime
UI -> MovieDB: Get showtime details
MovieDB -> MovieDB: Retrieve showtime details
MovieDB --> UI: Return showtime details

==Seat Reservation==
User -> UI: Select seats
UI -> Reservation: Reserve seats
Reservation -> Reservation: Check seat availability
Reservation --> UI: Return available seats
Reservation -> Reservation: Create reservation
Reservation --> UI: Reservation created

==Payment==
User -> UI: Proceed to checkout
UI -> Payment: Make payment
Payment -> Payment: Process payment
Payment --> UI: Payment successful

==Administrator add movie==
AdminUI -> Administrator: Log in
AdminUI -> Administrator: Access admin panel
Administrator -> MovieDB: Add movie
MovieDB -> Administrator: Confirm movie details
Administrator -> MovieDB: Add movie to database
MovieDB -> MovieDB: Update movie showtimes
MovieDB --> AdminUI: Movie added successfully

==Administrator remove movie==
AdminUI -> Administrator: Log in
AdminUI -> Administrator: Access admin panel
Administrator -> MovieDB: Remove movie
MovieDB -> Administrator: Confirm movie details for removal
Administrator -> MovieDB: Remove movie from database
MovieDB -> MovieDB: Update movie showtimes
MovieDB --> AdminUI: Movie removed successfully

@enduml
